import {
  getAllNotifications,
  deleteNotificationsByIds,
  addNewNotification
} from '../repositories/notificationRepository';

export async function getNotifications(ctx) {
  const data = await getAllNotifications(ctx);
  ctx.body = {data, success: true};
}

export async function updateNotification(ctx) {
  ctx.body = {data, success: true};
}

export async function createNotification(ctx) {
  const data = await addNewNotification(ctx);
  ctx.body = {data, success: true};
}

export async function deleteNotifications(ctx) {
  const data = await deleteNotificationsByIds(ctx.req.query.ids.split(','));
  ctx.body = {data, success: true};
}
