import {
  getSettingsByShop,
  updateSettingsByShop,
  getSettingsByShopDomain
} from '../repositories/settingRepository';

export async function getSettings(ctx) {
  const data = await getSettingsByShop(ctx);
  ctx.body = {data, success: true};
}

export async function updateSettings(ctx) {
  const data = await updateSettingsByShop(ctx);
  ctx.body = {data, success: true};
}

export async function getSettingsByDomain(domain) {
  const data = await getSettingsByShopDomain(domain);
  ctx.body = {data, success: true};
}
