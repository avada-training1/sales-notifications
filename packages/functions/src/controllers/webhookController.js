import { createNewNotification } from '../services/webhookService';

/**
 * 
 * @param {*} ctx 
 */
export async function orderCreate(ctx) {
  const shopDomain = ctx.get('x-shopify-shop-domain');
  const orderData = ctx.req.body;
  const createdNoti = await createNewNotification({order: orderData, shopDomain})
  ctx.body = { success: true, data: createdNoti };
}
