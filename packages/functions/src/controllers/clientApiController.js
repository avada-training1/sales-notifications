import {getAllNotificationsByShopDomain} from '../repositories/notificationRepository';
import {getSettingsByShopDomain} from '../repositories/settingRepository';

export async function getNotifications(ctx) {
  try {
    const shopDomain = ctx.query.shop;
    const [settings, notifications] = await Promise.all([
      getSettingsByShopDomain(shopDomain),
      getAllNotificationsByShopDomain(shopDomain)
    ]);
    ctx.body = {
      settings,
      notifications
    };
  } catch (e) {
    ctx.body = {
      success: false,
      message: e.message
    };
  }
}
