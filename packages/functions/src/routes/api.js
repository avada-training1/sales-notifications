import Router from 'koa-router';
import * as sampleController from '@functions/controllers/sampleController';
import * as shopController from '@functions/controllers/shopController';
import * as subscriptionController from '@functions/controllers/subscriptionController';
import * as appNewsController from '@functions/controllers/appNewsController';
import * as notifications from '@functions/controllers/notificationsController';
import * as settings from '@functions/controllers/settingController';
import {getApiPrefix} from '@functions/const/app';

export default function apiRouter(isEmbed = false) {
  const router = new Router({prefix: getApiPrefix(isEmbed)});

  router.get('/samples', sampleController.exampleAction);
  router.get('/shops', shopController.getUserShops);
  router.get('/subscription', subscriptionController.getSubscription);
  router.get('/appNews', appNewsController.getList);

  router.get('/notifications', notifications.getNotifications);
  router.post('/notifications', notifications.createNotification);
  router.put('/notifications', notifications.updateNotification);
  router.delete('/notifications', notifications.deleteNotifications);

  router.get('/settings', settings.getSettings);
  router.put('/settings', settings.updateSettings);

  return router;
}
