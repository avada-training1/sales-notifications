import {Firestore} from '@google-cloud/firestore';
import {getCurrentShop} from '@functions/helpers/auth';
import {getShopById} from '@functions/repositories/shopRepository';

const firestore = new Firestore();
/** @type CollectionReference */
const collection = firestore.collection('settings');

/**
 * @returns {Object}
 */
export async function getSettingsByShop(ctx) {
  const {shopifyDomain} = await getShopById(getCurrentShop(ctx));
  const snapshot = await collection.where('shopDomain', '==', shopifyDomain).get();
  const settings = snapshot.docs.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
  return settings[0];
}

/**
 * @param {String} shopifyDomain
 * @returns {Object}
 */
export async function getSettingsByShopDomain(shopifyDomain) {
  const snapshot = await collection.where('shopDomain', '==', shopifyDomain).get();
  const settings = snapshot.docs.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
  return settings[0];
}

/**
 * @returns {Object}
 */
export async function updateSettingsByShop(ctx) {
  const settings = ctx.req.body;
  const querySnapshot = await collection.get();
  await querySnapshot.docs[0].ref.update({...settings});
  return settings[0];
}
