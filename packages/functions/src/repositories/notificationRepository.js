import {Firestore} from '@google-cloud/firestore';
import {getCurrentShop} from '@functions/helpers/auth';
import {getShopById} from '@functions/repositories/shopRepository';
import {fromNow} from '../helpers/utils/relativeTime';
const firestore = new Firestore();

/** @type CollectionReference */
const collection = firestore.collection('notifications');
/**
 * 
 * @returns {Array}
 */
export async function getAllNotifications(ctx) {
  const {shopifyDomain} = await getShopById(getCurrentShop(ctx));
  const snapshot = await collection.where('shopDomain', '==', shopifyDomain).get();
  const notifications = snapshot.docs.map(doc => ({
    id: doc.id,
    ...doc.data(),
    relativeDate: fromNow(doc.data().timestamp)
  }));

  notifications.sort((curr, next) => {
    return new Date(next.timestamp) - new Date(curr.timestamp);
  });

  return notifications;
}

/**
 * @param {string} shopDomain
 * @returns {Array}
 */
export async function getAllNotificationsByShopDomain(shopDomain) {
  const snapshot = await collection.where('shopDomain', '==', shopDomain).get();
  const notifications = snapshot.docs.map(doc => ({
    id: doc.id,
    ...doc.data(),
    relativeDate: fromNow(doc.data().timestamp)
  }));

  notifications.sort((curr, next) => {
    return new Date(next.timestamp) - new Date(curr.timestamp);
  });

  return notifications;
}

/**
 * @param {string} shopifyDomain
 * @returns {Object}
 */
export async function updateNotificationByShopDomain() {
  return {success: true, data: notifications};
}

/**
 * @param {Object} notificationData
 * @returns {Object}
 */

export async function addNewNotification(notificationData) {
  await collection.add(notificationData);
  return notificationData;
}

/**
 * @param {Array} ids
 * @returns {Array}
 */
export async function deleteNotificationsByIds(ids) {
  await Promise.all(ids.map(id => collection.doc(`${id}`).delete()));
  return ids.join(',');
}
