import {registerWebhooks} from '../../services/webhookService';
import {ordersToNotifications} from '../../services/notificationsService';

/**
 * 
 * @param {*} ctx 
 */
export default async function handleAfterInstall(ctx) {
  const shopifyDomain = ctx.state.shopify.shop;
  await Promise.all([
    registerWebhooks(shopifyDomain),
    ordersToNotifications(shopifyDomain)
  ])
}
