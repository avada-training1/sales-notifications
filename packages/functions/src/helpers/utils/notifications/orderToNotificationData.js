/**
 * 
 * @param {*} order 
 * @param {*} shopDomain 
 * @returns 
 */
export default function orderToNotification(order, shopDomain) {
  const customer = order.customer || {};
  const address = customer.default_address || {};
  const product = order.line_items[0];
  const notification = {
    firstName: customer.first_name || 'Anonymous',
    city: address.city || '',
    productName: product.name,
    country: address.country || '',
    timestamp: order.created_at,
    productImage: '',
    productId: product.product_id,
    shopDomain: shopDomain
  };
  
  return notification;
}
