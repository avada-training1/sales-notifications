/**
 * Returns the list of products width image
 * @returns {Array}
 */
export default async function getProductImagesByIds(shopify, ids) {
  const query = `query Products($ids: [ID!]!) {
        nodes(ids: $ids) {
          ... on Product {
            id
            title
            images (first: 1){
              nodes {
                url(transform: {maxHeight: 80, maxWidth: 80})
              }
            }
          }
        }
    }`;

  const products = await shopify.graphql(query, { ids });

  const productImages = new Map();
  products.nodes.map(product => productImages.set(product.id.split('/')[4], product.images.nodes[0]?.url || '')
  );
  return productImages;
}
