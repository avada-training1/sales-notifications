export default function getImagebyProductId(id, products) {
  return products.find(product => product.id === id).image;
  // for (const product of products) {
    // if (product.id.includes(id)) return product.image;
  // }
}
