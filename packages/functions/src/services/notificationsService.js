import Shopify from 'shopify-api-node';
import { getShopByShopDomain } from '../repositories/shopRepository';
import getProductImagesByIds from '../helpers/utils/notifications/getProductImagesByIds';
import { Firestore } from '@google-cloud/firestore';
import orderToNotification from '../helpers/utils/notifications/orderToNotificationData';

const firestore = new Firestore();
const collection = firestore.collection('notifications');

const MAX_SYNC_ORDERS = 30;

/**
 * 
 * @param {Object} notificationsData 
 * @param {*} shopify 
 * @returns {Object}
 */
export async function addNewNotifications(notificationsData, shopify) {
  const ids = [...new Set(notificationsData.map(notification => `gid://shopify/Product/${notification.productId}`))];
  const productImages = await getProductImagesByIds(shopify, ids);
  const addedNotifications = notificationsData.map(notification => ({
    ...notification,
    productImage: productImages.get(`${notification.productId}`)
  }));
  await Promise.all(addedNotifications.map(notification => collection.add(notification)));
  return addedNotifications;
}

/**
 * 
 * @param {String} shopDomain 
 */
export async function ordersToNotifications(shopDomain) {
  const shop = await getShopByShopDomain(shopDomain);
  const shopify = new Shopify({
    shopName: shopDomain,
    accessToken: shop.accessToken
  });
  const orders = await shopify.order.list({ limit: MAX_SYNC_ORDERS });
  const notificationsData = orders.map(order => orderToNotification(order, shopDomain));
  await addNewNotifications(notificationsData, shopify);
}
