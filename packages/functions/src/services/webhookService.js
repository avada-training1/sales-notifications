import Shopify from 'shopify-api-node';
import {getShopByShopDomain} from '../repositories/shopRepository';
import {addNewNotifications} from '../services/notificationsService';

const WEBHOOK_CALLBACK_URL = 'https://5b98-171-224-181-234.ngrok-free.app/webhook';
const API_VERSION = '2024-01';

export async function registerWebhooks(shopifyDomain) {
  const shop = await getShopByShopDomain(shopifyDomain);
  const shopify = new Shopify({
    shopName: shopifyDomain,
    accessToken: shop.accessToken,
    apiVersion: API_VERSION
  });
  // register webhook
  const listWebhooks = await shopify.webhook.list();
  const hasWebhookAlready = listWebhooks.filter(webhook => webhook.topic === 'orders/create').length === 0;
  if (hasWebhookAlready) {
    const webhook = await shopify.webhook.create({
      topic: 'orders/create',
      format: 'json',
      address: `${WEBHOOK_CALLBACK_URL}/order/create`
    });
    console.log('Order create webhooks is created!', webhook);
  } else {
    console.log('Order create webhooks is existed!', listWebhooks);
  }
  // shopify.webhook.delete("1172222410889");
  // shopify.webhook.delete("1172222443657");
  // shopify.webhook.delete("1172265631881");
}

/**
 * 
 * @param {*} ctx 
 * @returns 
 */
export async function createNewNotification({order, shopDomain, shop = null}) {
  const shop = shop ? shop : await getShopByShopDomain(shopDomain);
  const shopify = new Shopify({
    shopName: shopDomain,
    accessToken: shop.accessToken,
    apiVersion: API_VERSION
  });
  const customer = order.customer || {};
  const address = customer.default_address || {};
  const product = order.line_items[0];
  const newNotification = {
    firstName: customer.first_name || 'Anonymous',
    city: address.city || '',
    productName: product.name,
    country: address?.country || '',
    timestamp: order.created_at,
    productImage: '',
    productId: product.product_id,
    shopDomain: shopDomain
  };

  return addNewNotifications([newNotification], shopify);
}
