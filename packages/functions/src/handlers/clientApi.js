import App from 'koa';
import clientApiRouter from '@functions/routes/clientApi';
import * as errorService from '@functions/services/errorService';
import cors from '@koa/cors';
const api = new App();
api.proxy = true;
const router = clientApiRouter();

api.use(cors());
api.use(router.allowedMethods());
api.use(router.routes());
api.on('error', errorService.handleError);

export default api;
