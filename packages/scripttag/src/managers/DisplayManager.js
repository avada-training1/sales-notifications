import {insertAfter} from '../helpers/insertHelpers';
import {render} from 'preact';
import React from 'preact/compat';
import NotificationPopup from '../components/NotificationPopup/NotificationPopup';

export default class DisplayManager {
  constructor() {
    this.notifications = [];
    this.settings = {};
  }
  async initialize({notifications, settings}) {
    this.notifications = notifications.slice(0, settings.maxPopsDisplay);
    this.settings = settings;
    if (!this.isAllowDisplayPopup()) return;
    this.insertContainer();
    await this.displayAllNotifications(this.notifications, this.settings);
  }

  sleep(s) {
    return new Promise(resolve => {
      setTimeout(() => resolve(), s * 1000);
    });
  }

  isNotAllowedUrl() {
    if (this.settings.excludedUrls) {
      const listExcludedUrls = this.settings.includedUrls.split('\n');
      if (listExcludedUrls.filter(url => window.location.href.includes(url)).length) return true;
      else return false;
    }
    return false;
  }

  isAllowedUrl() {
    if (this.settings.includedUrls) {
      const listIncludedUrls = this.settings.includedUrls.split('\n');
      const formatedUrls = listIncludedUrls.map(url => url.split('?')[0]);
      if (formatedUrls.filter(url => window.location.href.includes(url)).length) return true;
    }
    return false;
  }

  isAllowDisplayPopup() {
    if (this.isNotAllowedUrl()) return false;
    if (this.settings.allowShow === 'specific' && !this.isAllowedUrl()) return false;
    return true;
  }

  async displayAllNotifications(notifications, settings) {
    if (Object.keys(this.settings).length) {
      await this.sleep(this.settings.firstDelay);
      for (const notification of notifications) {
        this.display({notification, settings});
        await this.sleep(this.settings.displayDuration);
        this.fadeOut();
        await this.sleep(this.settings.popsInterval);
      }
    }
  }

  applyDisplaySettings() {
    if (Object.keys(this.settings).length) {
      this.applyCustomPosition();
    }
  }

  fadeOut() {
    const container = document.querySelector('#Avada-SalePop');
    container.style.display = 'none';
  }

  display({notification, settings}) {
    const container = document.querySelector('#Avada-SalePop');
    container.style.display = 'block';
    render(<NotificationPopup {...{...notification, settings}} />, container);
  }

  insertContainer() {
    const popupEl = document.createElement('div');
    popupEl.id = `Avada-SalePop`;
    popupEl.classList.add('Avada-SalePop__OuterWrapper');
    const targetEl = document.querySelector('body').firstChild;
    if (targetEl) {
      insertAfter(popupEl, targetEl);
    }

    return popupEl;
  }
}
