import makeRequest from '../helpers/api/makeRequest';

export default class ApiManager {
  getNotifications = async () => {
    return this.getApiData();
  };

  getApiData = async () => {
    const shopifyDomain = window.Shopify.shop;
    const {notifications, settings} = await makeRequest(
      `http://localhost:5050/client-api/notifications?shop=${shopifyDomain}`
    );
    return {notifications, settings};
  };
}
