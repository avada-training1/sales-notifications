import React, {Suspense} from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from '@assets/loadables/Home/Home';
import NotFound from '@assets/loadables/NotFound/NotFound';
import Samples from '@assets/loadables/Samples/Samples';
import Notifications from '@assets/pages/Notifications/Notifications';
import Settings from '@assets/loadables/Settings/Settings';
import {routePrefix} from '@assets/config/app';
import Loading from '@assets/components/Loading';

const Routes = ({prefix = routePrefix}) => {
  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        <Route exact path={prefix + '/'} component={Home} />
        <Route exact path={prefix + '/notifications'} component={Notifications} />
        <Route exact path={prefix + '/settings'} component={Settings} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Suspense>
  );
};

export default Routes;
