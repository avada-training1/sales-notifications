import {POPUP_POSITION} from '../components/DesktopPosition/DesktopPosition';

export const settings = {
  position: POPUP_POSITION.BOTTOM_LEFT,
  hideTimeAgo: false,
  truncateProductName: false,
  displayDuration: 5,
  firstDelay: 5,
  popsInterval: 5,
  maxPopsDisplay: 20,
  allowShow: 'all',
  includedUrls: '',
  excludedUrls: ''
};
