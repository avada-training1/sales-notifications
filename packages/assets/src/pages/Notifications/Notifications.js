import {
  LegacyCard,
  ResourceList,
  ResourceItem,
  Page,
  InlineStack,
  Pagination
} from '@shopify/polaris';
import {DeleteIcon} from '@shopify/polaris-icons';
import {useCallback, useEffect, useState} from 'react';
import useFetchApi from '@assets/hooks/api/useFetchApi';
import NotificationPopup from '@assets/components/NotificationPopup/NotificationPopup';
import {api} from '../../helpers';

const Notifications = () => {
  const [sortValue, setSortValue] = useState('DATE_MODIFIED_DESC');
  const [selectedItems, setSelectedItems] = useState([]);
  const {data: notifications, loading, setData: setNotifications} = useFetchApi({
    url: `/notifications?sort=${sortValue}`
  });
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);

  const resourceName = {
    singular: 'notification',
    plural: 'notifications'
  };

  const handleDeleteNotifications = useCallback(async () => {
    setIsLoading(true);
    const res = await api(`/notifications?ids=${selectedItems.join(',')}`, {
      method: 'DELETE'
    });
    if (res.success) {
      const newNotifications = [...notifications];
      const length = newNotifications.length;
      const deletedNotifications = res.data.split(',');
      for (let i = length - 1; i >= 0; i--) {
        if (deletedNotifications.includes(newNotifications[i].id)) {
          newNotifications.splice(i, 1);
        }
      }
      setNotifications(newNotifications);
    }
    setIsLoading(false);
  }, [selectedItems, notifications]);

  const handleChangeOrderCreated = useCallback(() => {
    const sortedNotifications = [...notifications];
    sortedNotifications.sort((curr, next) => {
      if (sortValue === 'DATE_MODIFIED_ASC')
        return new Date(curr.timestamp) - new Date(next.timestamp);
      return new Date(next.timestamp) - new Date(curr.timestamp);
    });
    setNotifications(sortedNotifications);
  }, [sortValue, notifications]);

  const bulkActions = [
    {
      icon: DeleteIcon,
      content: 'Delete notifications',
      destructive: true,
      onAction: () => handleDeleteNotifications()
    }
  ];

  useEffect(() => {
    handleChangeOrderCreated();
  }, [sortValue, loading]);

  return (
    <Page title={'Notifications'} subtitle={'List of sales notifcation from Shopify'}>
      <LegacyCard>
        <ResourceList
          loading={loading || isLoading}
          resourceName={resourceName}
          items={notifications.slice(10 * page, 10 * page + 10) || []}
          renderItem={renderItem}
          sortValue={sortValue}
          bulkActions={bulkActions}
          selectedItems={selectedItems}
          onSelectionChange={setSelectedItems}
          sortOptions={[
            {label: 'Newest update', value: 'DATE_MODIFIED_DESC'},
            {label: 'Oldest update', value: 'DATE_MODIFIED_ASC'}
          ]}
          onSortChange={selected => {
            setSortValue(selected);
          }}
        />
        <br />
      </LegacyCard>
      <br />
      <InlineStack align="center">
        <Pagination
          hasPrevious={page > 0}
          onPrevious={() => {
            setPage(page => {
              if (page > 0) return page - 1;
              return page;
            });
          }}
          hasNext={page < notifications.length / 10 - 1}
          onNext={() => {
            setPage(page => {
              if (page < notifications.length / 10 - 1) return page + 1;
              return page;
            });
          }}
        />
      </InlineStack>
    </Page>
  );

  function renderItem(item) {
    return (
      <ResourceItem id={item.id}>
        <NotificationPopup {...item} />
      </ResourceItem>
    );
  }
};

export default Notifications;
