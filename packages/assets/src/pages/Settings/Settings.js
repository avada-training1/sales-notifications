import React, { useCallback, useEffect, useMemo, useState } from 'react';
import {
  Layout,
  Page,
  LegacyCard,
  Tabs,
  SkeletonTabs,
  SkeletonDisplayText,
  Text,
  BlockStack,
  Checkbox,
  RangeSlider,
  TextField,
  InlineGrid,
  Select,
  InlineStack,
  SkeletonThumbnail,
  SkeletonBodyText
} from '@shopify/polaris';
import NotificationPopup from '@assets/components/NotificationPopup/NotificationPopup';
import DesktopPosition from '../../components/DesktopPosition/DesktopPosition';
import { useStore } from '../../reducers/storeReducer';
import useFetchApi from '@assets/hooks/api/useFetchApi';
import { api } from '../../helpers';
import { setToast } from '@assets/actions/storeActions';

/**
 * @return {JSX.Element}
 */
export default function Settings() {
  const { data: settingsFirebase, loading, setData: setSettingsFirebase } = useFetchApi({ url: '/settings' });
  const { dispatch } = useStore();
  const [isLoading, setIsLoading] = useState(false);
  const [selected, setSelected] = useState(0);
  const handleTabChange = useCallback(selectedTabIndex => setSelected(selectedTabIndex), []);

  const handleChangeInputValue = useCallback(
    (key, value) => {
      setSettingsFirebase({
        ...settingsFirebase,
        [key]: value
      })
    },
    [settingsFirebase]
  );

  const handleUpdateDisplaySettings = useCallback(async () => {
    try {
      setIsLoading(true);
      await api('/settings', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: settingsFirebase
      });
      setToast(dispatch, 'Update settings successfully');
    } catch (e) {
      console.error(e);
      setToast(dispatch, 'Update settings failed', true);
    } finally {
      setIsLoading(false);
    }
  }, [settingsFirebase]);

  const tabs = [
    {
      id: 'display-tab',
      content: 'Display',
      panelID: 'display-tab-1',
      value: <LegacyCard.Section>
        <BlockStack gap={'400'}>
          <Text as="span" variant="headingSm">
            APPEARANCE
          </Text>
          <DesktopPosition position={settingsFirebase.position} setPosition={handleChangeInputValue} />
          <Checkbox
            label="Hide time ago"
            onChange={() => handleChangeInputValue('hideTimeAgo', !settingsFirebase.hideTimeAgo)}
            checked={settingsFirebase.hideTimeAgo}
          />
          <Checkbox
            label="Truncate content text"
            helpText="If your product name is long for one line, it will be truncated to 'Product na...'"
            onChange={() =>
              handleChangeInputValue('truncateProductName', !settingsFirebase.truncateProductName)
            }
            checked={settingsFirebase.truncateProductName}
          />
          <Text as="span" variant="headingSm">
            TIMING
          </Text>
          <InlineGrid columns={2} gap="400">
            <BlockStack>
              <Text>Display duration</Text>
              <InlineGrid columns={['twoThirds', 'oneThird']} alignItems="center" gap="200">
                <RangeSlider
                  max={20}
                  label=""
                  value={settingsFirebase["displayDuration"]}
                  onChange={value => handleChangeInputValue("displayDuration", value)}
                />
                <TextField
                  label=""
                  autoComplete="off"
                  suffix={'second(s)'}
                  value={settingsFirebase["displayDuration"]}
                  onChange={value => handleChangeInputValue("displayDuration", value)}
                />
              </InlineGrid>
              <Text as="span" variant="bodySm">
                How long each pop will display on your page.
              </Text>
            </BlockStack>
            <BlockStack>
              <Text>Time before the first pop</Text>
              <InlineGrid columns={['twoThirds', 'oneThird']} alignItems="center" gap="200">
                <RangeSlider
                  max={20}
                  label=""
                  value={settingsFirebase["firstDelay"]}
                  onChange={value => handleChangeInputValue("firstDelay", value)}
                />
                <TextField
                  label=""
                  autoComplete="off"
                  suffix={'second(s)'}
                  value={settingsFirebase["firstDelay"]}
                  onChange={value => handleChangeInputValue("firstDelay", value)}
                />
              </InlineGrid>
              <Text as="span" variant="bodySm">
                The time before the first notification.
              </Text>
            </BlockStack>
          </InlineGrid>
          <InlineGrid columns={2} gap="400">
            <BlockStack>
              <Text>Gap time between two pops</Text>
              <InlineGrid columns={['twoThirds', 'oneThird']} alignItems="center" gap="200">
                <RangeSlider
                  max={20}
                  label=""
                  value={settingsFirebase["popsInterval"]}
                  onChange={value => handleChangeInputValue("popsInterval", value)}
                />
                <TextField
                  label=""
                  autoComplete="off"
                  suffix={'second(s)'}
                  value={settingsFirebase["popsInterval"]}
                  onChange={value => handleChangeInputValue("popsInterval", value)}
                />
              </InlineGrid>
              <Text as="span" variant="bodySm">
                The time interval between two popup notifications.
              </Text>
            </BlockStack>
            <BlockStack>
              <Text>Maximum of popups</Text>
              <InlineGrid columns={['twoThirds', 'oneThird']} alignItems="center" gap="200">
                <RangeSlider
                  max={80}
                  label=""
                  value={settingsFirebase["maxPopsDisplay"]}
                  onChange={value => handleChangeInputValue("maxPopsDisplay", value)}
                />
                <TextField
                  label=""
                  autoComplete="off"
                  value={settingsFirebase["maxPopsDisplay"]}
                  onChange={value => handleChangeInputValue("maxPopsDisplay", value)}
                />
              </InlineGrid>
              <Text as="span" variant="bodySm">
                The maximum number of popups are allowed to show after page loading. Maximum number is 80.
              </Text>
            </BlockStack>
          </InlineGrid>
        </BlockStack>
      </LegacyCard.Section>
    },
    {
      id: 'triggers-tab',
      content: 'Triggers',
      panelID: 'triggers-tab-1',
      value: <LegacyCard.Section>
        <BlockStack gap={'400'}>
          <Select
            label={
              <Text as="span" variant="headingSm">
                PAGES RESTRICTION
              </Text>
            }
            options={[
              {
                label: 'All pages',
                value: 'all'
              },
              {
                label: 'Specific pages',
                value: 'specific'
              }
            ]}
            value={settingsFirebase.allowShow}
            onChange={value => handleChangeInputValue('allowShow', value)}
          />
          {settingsFirebase.allowShow === 'specific' && (
            <TextField
              label="Included pages"
              autoComplete="off"
              multiline={5}
              value={settingsFirebase.includedUrls}
              onChange={value => {
                handleChangeInputValue('includedUrls', value);
              }}
            />
          )}
          <TextField
            label="Excluded pages"
            autoComplete="off"
            multiline={5}
            value={settingsFirebase.excludedUrls}
            onChange={value => {
              handleChangeInputValue('excludedUrls', value);
            }}
          />
        </BlockStack>
      </LegacyCard.Section>
    }
  ]

  const SkeletonSettings = () =>
    useMemo(
      () => (
        <Layout.Section>
          <LegacyCard sectioned>
            <BlockStack gap={'400'}>
              <SkeletonTabs />
              <SkeletonDisplayText size="medium" />
              <InlineStack gap={'1000'}>
                <SkeletonThumbnail size="large" />
                <SkeletonThumbnail size="large" />
                <SkeletonThumbnail size="large" />
                <SkeletonThumbnail size="large" />
              </InlineStack>
              <SkeletonThumbnail size="extraSmall" />
              <SkeletonThumbnail size="extraSmall" />
              <SkeletonBodyText lines={10} />
            </BlockStack>
          </LegacyCard>
        </Layout.Section>
      ),
      []
    );

  return (
    <Page
      fullWidth
      title="Settings"
      subtitle="Decide how your notifications will display"
      primaryAction={{
        loading: isLoading,
        content: 'Save',
        onAction: () => {
          handleUpdateDisplaySettings();
        }
      }}
    >
      <Layout>
        <Layout.AnnotatedSection description={<NotificationPopup hideTimeAgo={settingsFirebase.hideTimeAgo} />}>
          <Layout.Section>
            {loading ? (
              <SkeletonSettings />
            ) : (
              <LegacyCard>
                <Tabs
                  tabs={tabs}
                  selected={selected}
                  onSelect={handleTabChange}
                />
                {tabs[selected].value}
              </LegacyCard>
            )}
          </Layout.Section>
        </Layout.AnnotatedSection>
      </Layout>
    </Page>
  );
}

Settings.propTypes = {};
