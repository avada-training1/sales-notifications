import {BlockStack, InlineStack, Text} from '@shopify/polaris';
import styles from './desktopPosition.module.css';

export const POPUP_POSITION = {
  BOTTOM_LEFT: 'bottom-left',
  BOTTOM_RIGHT: 'bottom-right',
  TOP_LEFT: 'top-left',
  TOP_RIGHT: 'top-right'
};
const DesktopPosition = ({position, setPosition}) => {
  const renderPosition = (selfPos = POPUP_POSITION.BOTTOM_LEFT) => {
    return (
      <InlineStack>
        <div
          className={`${styles['desktop-screen']} ${
            position !== selfPos ? styles['desktop-screen--unactive'] : ''
          }`}
          onClick={() => {
            setPosition('position', selfPos)
          }}
        >
          <div
            className={`${styles[selfPos]} ${
              position !== selfPos ? styles['position--unactive'] : ''
            }`}
          ></div>
        </div>
      </InlineStack>
    );
  };
  return (
    <BlockStack>
      <Text>Desktop Position</Text>
      <InlineStack gap={'200'}>
        {renderPosition(POPUP_POSITION.BOTTOM_LEFT)}
        {renderPosition(POPUP_POSITION.BOTTOM_RIGHT)}
        {renderPosition(POPUP_POSITION.TOP_LEFT)}
        {renderPosition(POPUP_POSITION.TOP_RIGHT)}
      </InlineStack>
      <Text as="span" variant="bodySm">
        The display position of the pop on your website.
      </Text>
    </BlockStack>
  );
};
DesktopPosition.propTypes = {};
export default DesktopPosition;
